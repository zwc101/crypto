# Crypto

This project contains a small demo of the Triple DES algorithm, written in Rust.  I hope to add other algorithms over time, and will be maintaining Rust.

This is entirely for educational purposes - for me to learn both cryptography and Rust.  Feel free to take or modify it, but I would never use this for anything serious and neither should you.  :) 