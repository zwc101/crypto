//Linting suppressions
#![allow(non_snake_case)] 
#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(non_upper_case_globals)]



//Expansion table
static ExpTable: [u32; 48] = [
                                        32, 01, 02, 03, 04, 05, 
                                        04, 05, 06, 07, 08, 09, 
                                        08, 09, 10, 11, 12, 13,
                                        12, 13, 14, 15, 16, 17,
                                        16, 17, 18, 19, 20, 21, 
                                        20, 21, 22, 23, 24, 25, 
                                        24, 25, 26, 27, 28, 29, 
                                        28, 29, 30, 31, 32, 01,
                             ];

//f function permutation table
static fPermuteTable: [u32; 32] = [
                                        16, 07, 20, 21, 29, 12, 28, 17,
                                        01, 15, 23, 26, 05, 18, 31, 10,
                                        02, 08, 24, 14, 32, 27, 03, 09,
                                        19, 13, 30, 06, 22, 11, 04, 25,
                                  ];

//Key Permute 1
static KeyPermuteTable: [u32; 56] = [
                                        57, 49, 41, 33, 25, 17, 09, 01,
                                        58, 50, 42, 34, 26, 18, 10, 02,
                                        59, 51, 43, 35, 27, 19, 11, 03,
                                        60, 52, 44, 36, 63, 55, 47, 39,
                                        31, 23, 15, 07, 62, 54, 46, 38,
                                        30, 22, 14, 06, 61, 53, 45, 37,
                                        29, 21, 13, 05, 28, 20, 12, 04,
                                    ];

//Key Permute 2
static KeyPermute2Table: [u32; 48] = [
                                        14, 17, 11, 24, 01, 05, 03, 28,
                                        15, 06, 21, 10, 23, 19, 12, 04,
                                        26, 08, 16, 07, 27, 20, 13, 02,
                                        41, 52, 31, 37, 47, 55, 30, 40,
                                        51, 45, 33, 48, 44, 49, 39, 56,
                                        34, 53, 46, 42, 50, 36, 29, 32,
                                     ];

//Initial Permutation table
static IPTable: [u32; 64] = [
                                        58, 50, 42, 34, 26, 18, 10, 02,
                                        60, 52, 44, 36, 28, 20, 12, 04,
                                        62, 54, 46, 38, 30, 22, 14, 06,
                                        64, 56, 48, 40, 32, 24, 16, 08,
                                        57, 49, 41, 33, 25, 17, 09, 01,
                                        59, 51, 43, 35, 27, 19, 11, 03,
                                        61, 53, 45, 37, 29, 21, 13, 05,
                                        63, 55, 47, 39, 31, 23, 15, 07,
                            ];

//Final Permutation table
static IPInvTable: [u32; 64] = [
                                        40, 08, 48, 16, 56, 24, 64, 32,
                                        39, 07, 47, 15, 55, 23, 63, 31,
                                        38, 06, 46, 14, 54, 22, 62, 30,
                                        37, 05, 45, 13, 53, 21, 61, 29,
                                        36, 04, 44, 12, 52, 20, 60, 28,
                                        35, 03, 43, 11, 51, 19, 59, 27,
                                        34, 02, 42, 10, 50, 18, 58, 26,
                                        33, 01, 41, 09, 49, 17, 57, 25,
                               ];

//SBoxes
static sbox: [[[u32; 16]; 4];8] =
            [
            //S1
                [ [14, 04, 13, 01, 02, 15, 11, 08, 03, 10, 06, 12, 05, 09, 00, 07],
                  [00, 15, 07, 04, 14, 02, 13, 01, 10, 06, 12, 11, 09, 05, 03, 08],
                  [04, 01, 14, 08, 13, 06, 02, 11, 15, 12, 09, 07, 03, 10, 05, 00],
                  [15, 12, 08, 02, 04, 09, 01, 07, 05, 11, 03, 14, 10, 00, 06, 13]],  
            //S2
                [ [15, 01, 08, 14, 06, 11, 03, 04, 09, 07, 02, 13, 12, 00, 05, 10],
                  [03, 13, 04, 07, 15, 02, 08, 14, 12, 00, 01, 10, 06, 09, 11, 05],
                  [00, 14, 07, 11, 10, 04, 13, 01, 05, 08, 12, 06, 09, 03, 02, 15],
                  [13, 08, 10, 01, 03, 15, 04, 02, 11, 06, 07, 12, 00, 05, 14, 09]],
            //S3
                [ [10, 00, 09, 14, 06, 03, 15, 05, 01, 13, 12, 07, 11, 04, 02, 08],
                  [13, 07, 00, 09, 03, 04, 06, 10, 02, 08, 05, 14, 12, 11, 15, 01],
                  [13, 06, 04, 09, 08, 15, 03, 00, 11, 01, 02, 12, 05, 10, 14, 07],
                  [01, 10, 13, 00, 06, 09, 08, 07, 04, 15, 14, 03, 11, 05, 02, 12]],
            //S4
                [ [07, 13, 14, 03, 00, 06, 09, 10, 01, 02, 08, 05, 11, 12, 04, 15],
                  [13, 08, 11, 05, 06, 15, 00, 03, 04, 07, 02, 12, 01, 10, 14, 09],
                  [10, 06, 09, 00, 12, 11, 07, 13, 15, 01, 03, 14, 05, 02, 08, 04],
                  [03, 15, 00, 06, 10, 01, 13, 08, 09, 04, 05, 11, 12, 07, 02, 14]],
            //S5
                [ [02, 13, 04, 01, 07, 10, 11, 06, 08, 05, 03, 15, 13, 00, 14, 09],
                  [14, 11, 02, 12, 04, 07, 13, 01, 05, 00, 15, 10, 03, 09, 08, 06],
                  [04, 02, 01, 11, 10, 13, 07, 08, 15, 09, 12, 05, 06, 03, 00, 14],
                  [11, 08, 12, 07, 01, 14, 02, 13, 06, 15, 00, 09, 10, 04, 05, 03]],
            //S6
                [ [12, 01, 10, 15, 09, 02, 06, 08, 00, 13, 03, 04, 14, 07, 05, 11],
                  [10, 15, 04, 02, 07, 12, 09, 05, 06, 01, 13, 14, 00, 11, 03, 08],
                  [09, 14, 15, 05, 02, 08, 12, 03, 07, 00, 04, 10, 01, 13, 11, 06],
                  [04, 03, 02, 12, 09, 05, 15, 10, 11, 14, 01, 07, 06, 00, 08, 13]],
            //S7
                [ [04, 11, 02, 14, 15, 00, 08, 13, 03, 12, 09, 07, 05, 10, 06, 01],
                  [13, 00, 11, 07, 04, 09, 01, 10, 14, 03, 05, 12, 02, 15, 08, 06],
                  [01, 04, 11, 13, 12, 03, 07, 14, 10, 15, 06, 08, 00, 05, 09, 02],
                  [06, 11, 13, 08, 01, 04, 10, 07, 09, 05, 00, 15, 14, 02, 03, 12]],
            //S8
                [ [13, 02, 08, 04, 06, 15, 11, 01, 10, 09, 03, 14, 05, 00, 12, 07],
                  [01, 15, 13, 08, 10, 03, 07, 04, 12, 05, 06, 11, 00, 14, 09, 02],
                  [07, 11, 04, 01, 09, 12, 14, 02, 00, 06, 10, 13, 15, 03, 05, 08],
                  [02, 01, 14, 07, 04, 10, 08, 13, 15, 12, 09, 00, 03, 05, 06, 11]]
            ];

//Applies a permutation table to a block
fn permute32BitBlock(block: u64, table: [u32; 32]) -> u64 {
    let mut index = 0;
    let mut result = 0;

    for pos in table.into_iter() {
        let pos = *pos;
        result = result ^ permute64Bit(block, index, pos);
        index += 1;
    }

    result
}

//Applies a permutation table to a block
fn permute56BitBlock(block: u64, table: [u32; 56]) -> u64 {
    let mut index = 0;
    let mut result = 0;

    for pos in table.into_iter() {
        let pos = *pos;
        result = result ^ permute64Bit(block, index, pos);
        index += 1;
    }

    result
}

//Applies a permutation table to a block
fn permute64BitBlock(block: u64, table: [u32; 64]) -> u64 {
    let mut index = 0;
    let mut result = 0;

    for pos in table.into_iter() {
        let pos = *pos;
        result = result ^ permute64Bit(block, index, pos);
        index += 1;
    }

    result
}

//Applies a permutation table to a block
fn permute48BitBlock(block: u64, table: [u32; 48]) -> u64 {
    let mut index = 0;
    let mut result = 0;

    for pos in table.into_iter() {
        let pos = *pos;
        result = result ^ permute64Bit(block, index, pos);
        index += 1;
    }

    result
}

//Expands a key into
fn expansionTransform(key: u32, table: Vec<u32>) -> u32 {
    let mut index = 0;
    let mut result = 0;

    for pos in table {
        result = result ^ permute32Bit(key, index, pos);
        index += 1;
    }

    result
}

//Moves the bit at srcIndex to destIndex in input
fn permute64Bit(input: u64, srcIndex: u32, destIndex: u32) -> u64 {
    let bit: u64 = (input >> srcIndex) & 1;
    input ^ (bit << destIndex - 1)
}

//Moves the bit at srcIndex to destIndex in input
fn permute32Bit(input: u32, srcIndex: u32, destIndex: u32) -> u32 {
    let bit: u32 = (input >> srcIndex) & 1;
    input ^ (bit << destIndex - 1)
}

fn generateKeySchedule(key: u64) -> Vec<u64> {
    let permutedKey = permute56BitBlock(key, KeyPermuteTable);
    println!("Permuted Key: 0X{:x}", permutedKey);
    let leftBitmask = 0xFFFFFFF0000000;
    let rightBitmask = 0xFFFFFFF;
    let rightKey = permutedKey & rightBitmask;
    let leftKey = permutedKey & leftBitmask;

    let mut C = rightKey;
    let mut D = leftKey;
    let mut keySet: Vec<u64> = Vec::new();
    for i in 0..16
    //Generate 16 round keys
    {
        //Certain rounds shift 2 bits
        if i == 0 || i == 1 || i == 8 || i == 15 {
            C = C << 2; //Shift 2
            C = (C >> 26) ^ C; //Copy old bits to end
            C = C & 0xFFFFFFF; //Remove two extra bits
            D = D << 2; //Shift 2
            D = (D >> 26) ^ D; //Copy old bits to end
            D = D & 0xFFFFFFF; //Remove two extra bits
        } else
        //Everything else needs 1 bit
        {
            C = C << 1; //Shift 1
            C = (C >> 27) ^ C; //Copy old bit to end
            C = C & 0xFFFFFFF; //Remove extra bit
            D = D << 1; //Shift 1
            D = (D >> 27) ^ D; //Copy old bit to end
            D = D & 0xFFFFFFF; //Remove extra bit
        }
        //Combine Keys
        let mut outputKey: u64 = 0x0;
        outputKey = outputKey ^ (C << 28);
        outputKey = outputKey ^ D;
        //Second permute
        outputKey = permute48BitBlock(outputKey, KeyPermute2Table);
        //Load into vector
        keySet.push(outputKey);
    }

    keySet
}

//The f function for the Feistal network
fn f(input: u64, key: u64) -> u64 {
    //Expand block
    let expandedInput: u64 = permute48BitBlock(input, ExpTable);
    //println!("Expanded: 0x{:b}", expandedInput);
    //XOR with key
    let XORedInput: u64 = expandedInput ^ key;
    //SBoxes
    let mut bitMask: u64 = 0x3F;
    //println!("bitmask1: {:b}", bitMask);

    let mut output: u64 = 0x00;
    for i in 0..6 {
        //Seperate out the current 6 bits
        let sixSet: u64 = (expandedInput & bitMask) >> i * 6;
        //println!("Byte {}: {:b}",i, sixSet);
        //Shift to next 6 bits
        bitMask = bitMask << 6;

        //Apply current sbox
        //Extract outer 2 bits
        let mut outerBits: u64 = 0;
        outerBits = outerBits ^ ((sixSet & 0x20) >> 4);
        outerBits = outerBits ^ (sixSet & 0x01);
        //Extract inner 4 bits
        let mut innerBits: u64 = 0;
        innerBits = innerBits ^ ((sixSet & 0x1E) >> 1);
        output = output ^ ((sbox[i][outerBits as usize][innerBits as usize] as u64) << 4 * i);
    }

    //Permute
    output = permute32BitBlock(output, fPermuteTable);
    output
}

fn main() {
    println!("\n\n");
    println!("==============================");
    println!("|          KEYGEN            |");
    println!("==============================");
    //Input key
    let key: u64 = 0x1dfb772505ceda78;
    println!("Original key: 0x{:x}", key);
    generateKeySchedule(key);

    println!("\n\n");
    println!("==============================");
    println!("|         ENCRYPTION         |");
    println!("==============================");

    //Input block
    let block: u64 = 0xCAFEBABEDEADBEEF;
    println!("Input block: 0x{:X}", block);

    //Initial Block Permute
    let permutedBlock = permute64BitBlock(block, IPTable);
    println!("Permuted block: 0x{:X}", permutedBlock);

    //Feistal Network
    let rightBitMask: u64 = 0xFFFFFFFF;
    let leftBitMask:  u64 = 0xFFFFFFFF00000000;
    //Split block
    let rightHalf: u64 = permutedBlock & rightBitMask;
    let leftHalf:  u64 = (permutedBlock & leftBitMask) >> 32;

    //Process Right half
    let R1: u64 = f(rightHalf, key);
    //XOR with left half & swap
    let temp: u64 = rightHalf;
    let rightHalf = leftHalf ^ R1;
    let leftHalf = temp;

    let restoredBlock: u64 = permute64BitBlock(permutedBlock, IPInvTable);
    println!("Restored block: 0x{:X}", restoredBlock);
}
